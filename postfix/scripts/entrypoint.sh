#!/bin/sh
  
set -e

SASLDB2=/etc/sasl2/sasldb2
POSTFIX_CERT=/etc/ssl/postfix/postfix.crt
POSTFIX_KEY=/etc/ssl/postfix/postfix.key

postconf -e maillog_file=/dev/stdout
postconf -e recipient_delimiter=+
postconf -e owner_request_special=no
postconf -e unknown_local_recipient_reject_code=550
postconf -e smtpd_milters=inet:rspamd:11332
postconf -e milter_default_action=accept

# PostSRSd support
postconf -e sender_canonical_maps=tcp:postsrsd:10001
postconf -e sender_canonical_classes=envelope_sender
postconf -e recipient_canonical_maps=tcp:postsrsd:10002
postconf -e recipient_canonical_classes=envelope_recipient,header_recipient

if [ "$POSTFIX_MYHOSTNAME" ]; then
	echo "Setting myhostname to: $POSTFIX_MYHOSTNAME"
	postconf -ve myhostname="$POSTFIX_MYHOSTNAME"
fi

if [ "$POSTFIX_MYNETWORKS" ]; then
	echo "Setting mynetworks to: $POSTFIX_MYNETWORKS"
	postconf -e mynetworks="$POSTFIX_MYNETWORKS"
else
    postconf -e mynetworks="127.0.0.0/8,10.0.0.0/8,172.16.0.0/12,192.168.0.0/16"
fi

# TLS Support
if [ ! -f "$POSTFIX_CERT" ] || [ ! -f "$POSTFIX_KEY" ]; then
	echo "No certificate and/or key found, generating a self signed certificate"
	openssl req -x509 -newkey rsa:4096 -keyout "$POSTFIX_KEY" \
		-out "$POSTFIX_CERT" -sha256 -days 3650 -nodes \
		-subj "/CN=$POSTFIX_MYHOSTNAME"
fi

if [ -f "$POSTFIX_CERT" ] && [ -f "$POSTFIX_KEY" ]; then
	echo "Found certificate, enabling TLS support"
	postconf -e smtpd_tls_cert_file="$POSTFIX_CERT"
	postconf -e smtpd_tls_key_file="$POSTFIX_KEY"
	postconf -e smtpd_tls_security_level=may
	postconf -e smtp_tls_security_level=may
fi

# SASL/Submission support
if [ -s "/etc/postfix/users.txt" ]; then
	mkdir -p "${SASLDB2%/*}"
	while read -r user password address; do
		echo "Adding SASLDB2 user: $user"
		printf '%s' "$password" | saslpasswd2 -f "$SASLDB2" "$user"
		printf '%s\t%s\n' "$user" "$address" >> /etc/postfix/virtual_alias_maps
		printf '%s ' "${user#*@}" >> /etc/postfix/virtual_alias_domains
	done < /etc/postfix/users.txt
	postconf -e virtual_alias_maps="lmdb:/etc/postfix/virtual_alias_maps"
	postconf -e virtual_alias_domains="/etc/postfix/virtual_alias_domains"
	postmap /etc/postfix/virtual_alias_maps
	cat <<- EOF > "${SASLDB2%/*}"/smtpd.conf
	pwcheck_method: auxprop
	auxprop_plugin: sasldb
	mech_list: PLAIN LOGIN CRAM-MD5 DIGEST-MD5
	EOF
	chown -R postfix:postfix "${SASLDB2%/*}"
	postconf -e smtpd_sasl_auth_enable=yes
	postconf -e broken_sasl_auth_clients=yes
	postconf -e smtpd_sasl_authenticated_header=yes
	postconf -e smtpd_sasl_security_options=noanonymous
	postconf -M submission/inet="submission inet n - n - - smtpd"
	postconf -P submission/inet/syslog_name=postfix/submission
	if [ -f "$POSTFIX_CERT" ] && [ -f "$POSTFIX_KEY" ]; then
		postconf -P submission/inet/smtpd_tls_security_level=encrypt
		postconf -P submission/inet/smtpd_tls_auth_only=yes
	else
		echo "WARNING: SASL support enabled without TLS!"
	fi
fi

newaliases

exec "$@"
