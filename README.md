# Mailforwarder

Mailforwarder is a set of docker containers to forward email send to a specific address/domain and forward it to another address/domain. This is not a regular mail server as it does not store any email local. This setup is particular useful when using a Gmail account on which you can enable [multiple aliases]([https://support.google.com/mail/answer/22370](https://support.google.com/mail/answer/22370)) for which you are able to send and receive email for.

## Services

### Postfix

Postfix is the MTA used to handle incoming and outgoing mail flow.

### Rspamd

Rspamd is used to filter spam messages and apply DKIM and related security features.

### Redis

Is the storage backend used by Rspamd

### PostSRSd

PostSRSd is used to rewrite the sender address of forwarded emails. This will allow SPF to function correctly

## Setup

### Basics

The idea behind this setup is to make it as simple as possible to run a forwarding/smtp service. The only thing needed to configure is a hostname for the SMTP server which will automatically set the domain name for both the MTA and for SRS rewrite. You also need to provide a list of users, passwords and addresses for which we will be accepting email and a destination address to forward the email to. With the user(email address) and password it will also generate a SASL2 database to which senders can authenticate to be able to send out email via its MTA.

### Users

Docker compose will mount a tab/space delimited file `config/users.txt` which consists of the following 3 columns:

    address/username password forward_address
    me@domain.tld mysecretpassword me@gmail.com
    you@domain.tld	yoursecretpassword you@gmail.com

### Host/domain name

Postfix and postSRSd need to have a valid hostname setup. Create a `.env` file next to the `docker-compose.yml` and add the following: `POSTFIX_MYHOSTNAME=host.domain.tld`. This will automatically set the host name and the domain name for the services.

### TLS and Lets Encrypt

If you want to use valid Lets encrypt certificates, you will need to generate them outside of this project and provide them as `config/ssl/postfix.crt, config/ssl/postfix.key` so Postfix can read them. In case you do not provide a valid certificate a self signed certificate and key will be created for you.

### DKIM

For each domain taken from the addresses in users.txt a DKIM private and public key will be generated and will be available from the `config/dkim` directory. The public part (DNS in its name) will need to be configured as a TXT field on your domain.

## Operation

### Running

After setting `POSTFIX_MYHOSTNAME` in `.env` and providing `config/users.txt` you can start the containers by running:

    docker compose up -d

### Ports and access

The postfix container will expose two port numbers, `25 (smtp)` and `578 (submission)`. Rspamd also provides a webinterface on port `11334` which you can monitor and possibly train the spam filter. It will be accessible without a password from any IP address from the private namespace.
