#!/bin/sh

CONFIG_PATH="/etc/rspamd/local.d"

if [ ! -f "$CONFIG_PATH/dkim_signing.conf" ]; then
	echo "Creating initial dkim config"
	cat <<- EOF > "$CONFIG_PATH"/dkim_signing.conf
	path = "$CONFIG_PATH/dkim/\$domain.\$selector.key";
	selector = "default";
	allow_username_mismatch = true;
	EOF
fi

while read -r user password address; do
	if [ ! -f "$CONFIG_PATH/dkim/${user#*@}.default.key" ]; then
		echo "Generating dkim key for domain: ${user#*@}"
		mkdir -p "$CONFIG_PATH"/dkim
		rspamadm dkim_keygen --domain "${user#*@}" --selector default \
			-k "$CONFIG_PATH"/dkim/"${user#*@}".default.key \
			> "$CONFIG_PATH"/dkim/"${user#*@}".default.DNS.txt
	fi
done < /etc/rspamd/users.txt

chown -R rspamd:rspamd "$CONFIG_PATH"/dkim

if [ ! -f "$CONFIG_PATH/greylist.conf" ]; then
	echo "Creating initial greylist.conf"
	cat <<- EOF > "$CONFIG_PATH"/greylist.conf
	servers = "redis:6379";
	EOF
fi

if [ ! -f "$CONFIG_PATH/redis.conf" ]; then
	echo "Creating initial redis.conf"
	cat <<- EOF > "$CONFIG_PATH"/redis.conf
	write_servers = "redis";
	read_servers = "redis";
	EOF
fi

if [ ! -f "$CONFIG_PATH/worker-controller.inc" ]; then
	echo "Creating initial worker-controller.inc"
        cat <<- EOF > "$CONFIG_PATH"/worker-controller.inc
	bind_socket = "0.0.0.0:11334";
	secure_ip = "127.0.0.0/8,10.0.0.0/8,172.16.0.0/12,192.168.0.0/16";
	EOF
fi

if [ ! -f "$CONFIG_PATH/worker-normal.inc" ]; then
	echo "Creating initial worker-normal.inc"
	cat <<- EOF > "$CONFIG_PATH"/worker-normal.inc
	enabled = false;
	EOF
fi

if [ ! -f "$CONFIG_PATH/worker-proxy.inc" ]; then
	echo "Creating initial worker-proxy.inc"
        cat <<- EOF > "$CONFIG_PATH"/worker-proxy.inc
	upstream "local" {
	  self_scan = yes;
	}
	bind_socket = 0.0.0.0:11332;
	EOF
fi


exec "$@"
