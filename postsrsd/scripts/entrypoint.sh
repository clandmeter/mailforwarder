#!/bin/sh

export SRS_DOMAIN=${POSTFIX_MYHOSTNAME#*.}

if [ ! -s "/etc/postsrsd/secret" ]; then
	echo "PostSRSd secret not found, generating new secret"
	dd if=/dev/urandom bs=18 count=1 status=none | base64 > \
		/etc/postsrsd/secret
fi

exec "$@"
